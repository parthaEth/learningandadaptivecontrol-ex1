### What is this repository for? ###

This repo is dedicated to track progress of exercise 01 of Optimal and Learning Control for Autonomous Robots(OLCAR) class. 

### How do I get set up? ###

* Please run 'main_*.m'
* To get an overview please read '2015_olcar_ex1_handout.pdf'
function [LQR_Controller, Cost] = LQR_Design(Model,Task)
% LQR_DESIGN Creates an LQR controller to execute the Task
% Controller: Controller structure
%             u(x) = theta([t]) ' * BaseFnc(x)
%             .theta:   Parameter matrix (each column is associated to one
%                       control input)
%             .BaseFnc
%             .time

%% Problem 1.1: Find optimal feedback gains according to an LQR controller
% Make use of the elements in Task.cost for linearization points and cost
% functions.

% Define the state input around which the system dynamics should be linearized
% An in between position from a simulation has been picked
x_lin = zeros(size(Task.start_x)); % the position of the craft doesn't effect the linearization. But orientation does.
u_lin = Task.cost.u_eq; % assume that most of the time the quadrotor flies similar to hovering (Fz != 0)
                                                                                              % NOT EQUAL TO
  
% The linearized system dynamics matrices A_lin B_lin describe the dynamics
% of the system around the equilibrium state. See exercise sheet Eq.(2) for 
% usage example
A_lin = Model.Alin{1}(x_lin,u_lin,Model.param.syspar_vec);
B_lin = Model.Blin{1}(x_lin,u_lin,Model.param.syspar_vec);

% Compute optimal LQR gain (see command 'lqr')
[K,~,~] = lqr(A_lin,B_lin,Task.cost.Q_lqr,Task.cost.R_lqr);


%% Design the actual controller using the optimal feedback gains K
% The goal of this task is to correctly fill the matrix theta.
LQR_Controller.BaseFnc = @Augment_Base;
LQR_Controller.time    = Task.start_time:Task.dt:(Task.goal_time-Task.dt);
% The quadrotor controller produces inputs u from a combination of 
% feedforward uff and feedback elements as follows:
%
%   u = [Fz, Mx, My, Mz]' = uff + K'(x_ref - x)
%                         = uff + K'x_ref - K'x
%                         = [uff + K'x_ref, -K' ]' * [1,x']'
%                         =        theta'          * BaseFnc
uff = Task.cost.u_eq;
x_ref = Task.goal_x;
theta = [uff + K*x_ref, -K]';

%lqr_type = 'goal_state';  % Choose 'goal_state or 'via_point'
lqr_type = 'via_point';

fprintf('LQR controller design type: %s \n', lqr_type);
Nt = ceil((Task.goal_time - Task.start_time)/Task.dt+1);   
switch lqr_type
    case 'goal_state'
        %% Problem 1.2: Drive system to Task.goal_x with LQR Gain + feedforward
        % Define equilibrium point x_eq correctly and use it to generate
        % feedforward torques (see exercise sheet Eq.(3)).
        % x_ref = ...          % reference point the controller tries to reach
        % theta = ...          % dimensions (13 x 4)
        
        % stack constant theta matrices for every time step Nt
        LQR_Controller.theta = repmat(theta,[1,1,Nt]);
        %   Ok if you see the output the path doesn't look really nice. Why
        %should it lose altitude for no reason. Look at the cost function
        %initially error in x is huge so small changes in z doesn't make a
        %difference but on the otherhand it helps reduce control effort.
        %   In line 17 we have u_lin = Task.cost.u_eq; Because we want to 
        % rech the goal state as precisely as we can and it is ood to have 
        % (after linearization) model is a better approximation of the true
        % model when it matters i.e when it is close to goal.
        %   In line uff = Task.cost.u_eq; So we do not get offset error.
        %   Finally if you would like to go it in a nice straight line
        %   without losing altitude tie the path closely enough with way
        %   points. Just like the below thing ('via_point').
        
%       Question 1: How does the controller behave with varying positions
%       of the Task.goal_x? 
        %   Closer goal is better reached. Less loss in altitude as error in z
        % is no more musked by error in x. (more precisely error in every
        % component of state gets equal attention of the controller) 
        
%       Question 2: What is the cause of the varying performance (model,
%       cost function,...)? 
        %   It is the costfunction which makes everything bad/good. If the
        % cost function is extremely high it calls for extreme maneuver and
        % deviates the plant significantly from its linearized model. This
        % can cause instabilty once the deviation is out side the region of
        % attrection of the controller.
        
%       Question 3: How do changes in Task.cost.Q_lqr and Task.cost.R_lqr
%       affect the behavior? 
        %   Increasing Q increases the penalty for state error so increasing
        % it generaly improves reference trackin but if it is too high it
        % can cause actuator satuaration and thereby driving the system in
        % to a state that differs significantly from the linear model and
        % cause insability.
        %   Increasing R reduces the control effort but results in a poor
        %   tracking of the states. Increasing R also smoothens the
        %   trajectory followed. It can also cause offset error.
    case 'via_point'  
        %% Problem 1.3: Drive system to Task.goal_x through via point p1.
        % (see exercise sheet Eq.(3))
        i=1;
        j=1;
        while true
            try
                p = eval(['Task.vp',int2str(j)]);
                if ~isequal(p,Task.vp2)
                    t1(i) = eval(['Task.vp',int2str(j),'_time']);
                    p1(:,i) = p;% steer towards this input until t1
                    i=i+1;
                end
                j=j+1;
            catch
                break;
            end
        end
        
        j=1;
        for t=1:Nt-1
           % varying theta matrices for every time step Nt
           if t<=floor(t1(j)/Task.dt)
               x_ref = p1(:,j);
           elseif j<length(t1)
               j=j+1;
               x_ref = p1(:,j);
           else
               x_ref = Task.goal_x;
           end
           [K,~,~] = lqr(A_lin,B_lin,Task.cost.Q_lqr,Task.cost.R_lqr);
           theta = [uff + K*x_ref, -K]'; % use the same uff as above it doesn't change
           LQR_Controller.theta(:,:,t) = theta;
        end
%        Question 4: How does a via-point change behavior of the LQR
%        controller? 
         % Via points gives the flexibility to include intermediate goals
         % and obstacle avoidance. But we have to hand code every via point
         % for obstacle avoidance.(There could be a higher level controller
         % as well) though. A too wiled via point selection can render the
         % system unstable.
         
%        Question 5: Why is the system capable of reaching further away
%        Task.goal_x states? 
         % This is because if we do not have via points then at the
         % starting point the error is enormous which causes the actuaters
         % to saturate and hence the changes in the state doesn't happen as
         % was thaught would be while designing the LQR controller. This
         % induces instability. Also numerical error in case of really far
         % away goal state is another possibility.
         
%        Question 6: Defining via points Task.vp and time Task.vp_time seem
%        to have a positive influence on the system behavior and increase
%        stability. What are the disadvantages? 
         % 1. We have to have a good idea of the optimal path beforehand.
         % For example if we want to conserve fuel while reaching to a goal
         % state it is not entirely clear without pre comuting the optimal
         % path how to chose viapoint and at what time and space interval
         % 2. In case of obstacle achieved by via point method may induce
         % instability. One example could be the via point 2 provided with
         % the example code.
        
    otherwise
        error('Unknown lqr_type');
        
end

% Calculate cost of rollout
sim_out = Quad_Simulator(Model, Task, LQR_Controller);
Cost = Calculate_Cost(sim_out, Task);

end


function x_aug = Augment_Base(t,x)
% AUGMENT_BASE(t,x) Allows to incorporate feedforward and feedback
%
%   Reformulating the affine control law allows incorporating
%   feedforward term in a feedback-like fashion:
%   u = [Fz, Mx, My, Mz]' = uff + K(x - x_ref)
%                         = uff - K*x_ref + K*x
%                         = [uff - K*x_ref, K ] * [1,x']'
%                         =        theta'       * BaseFnc 

number_of_states = size(x,2);      % multiple states x can be augmented at once
x_aug = [ones(1,number_of_states); % augment row of ones
                    x           ];
end


function Cost = Calculate_Cost(sim_out, Task)
% Calculate_Cost(.) Asses the cost of a rollout sim_out 
%
%   Be sure to correctly define the equilibrium state (X_eq, U_eq) the 
%   controller is trying to reach.
X = sim_out.x;
U = sim_out.u;
Q = Task.cost.Q_lqr;
R = Task.cost.R_lqr;
X_eq = repmat(Task.cost.x_eq,1,size(X,2)-1); % equilibrium state LQR controller tries to reach
U_eq = repmat(Task.cost.u_eq,1,size(U,2));   % equilibrium input LQR controller tries to reach
Ex = X(:,1:end-1) - X_eq;                    % error in state
Eu = U - U_eq;                               % error in input

Cost = Task.dt * sum(sum(Ex.*(Q*Ex),1) + sum(Eu.*(R*Eu),1));
end





